#!/bin/bash

which radamsa || { echo "you don't have enough radamsa"; exit 1; }

HERE=`pwd`

make
test -d tmp || mkdir tmp && cd tmp || exit 1

test -x ../bin/blab || { echo "I can't see ../bin/blab"; exit 1; }

ulimit -S -t 1000     # max 1000s
ulimit -S -m 4194304  # 4GB max
ulimit -S -v 4194304  # 4GB max virtual

while true
do
   ../bin/blab -l ../lib -n 100 -e blab -v -o $$-blab-%n.blab
   radamsa -o $$-rad-b%n.blab -n 500 *.blab
   radamsa -o $$-rad-l%n.blab -n 500 ../lib/*.blab

   for foo in $$-*.blab
   do
      echo "testing $foo"
      ../bin/blab -n 0 $foo > /dev/null
      # this may crash validly due to memory or time limit for exponential cases
      RVAL=$?
      # copy to blab dir if interesting
      test $RVAL -gt 127 && echo "FAIL at $foo" && cp "$foo" "../blab-crash-$RVAL-$foo"
      rm $foo
   done
done
