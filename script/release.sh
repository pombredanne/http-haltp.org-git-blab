#!/bin/bash

set -e

make

DIR=`bin/blab --version | tr '[A-Z]' '[a-z]' | sed -e 's/ /-/g'`

echo "Target is $DIR"

test -d $DIR && rm -rf $DIR

mkdir -p $DIR/{bin,doc,lib,tests}
cp tests/* $DIR/tests
cp lib/*.blab $DIR/lib
cp doc/*.1 $DIR/doc
cp blab.l blab.c $DIR
cp README Makefile $DIR
tar -f - -c $DIR | gzip -9 > $DIR.tar.gz
cd $DIR
make && echo target built ok 
